import React from "react";
import { translationAdd } from "../api/user";
import TranslationForm from "../components/Translations/TranslationForm";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import withAuth from "../hoc/withAuth";
import { useUser } from "../store/UserContext";
import { storageSave } from "../utils/storage";

const Translations = () => {
  const { user, setUser } = useUser();

  const handleTranslateClick = async (wordToSign) => {
    const translate = wordToSign.trim();
    console.log(user);
    const [error, updatedUser] = await translationAdd(user, translate);
    if (error !== null) {
      return;
    }

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
    console.log("Error", error);
    console.log("updatedUser", updatedUser);
  };

  return (
    <>
      <section id="translate-word">
        <TranslationForm onTranslate={handleTranslateClick} />
      </section>
      <section></section>
    </>
  );
};

export default withAuth(Translations);
