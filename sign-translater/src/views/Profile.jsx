import { useEffect } from "react";
import { userById } from "../api/user";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../store/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

const Profile = () => {
  const { user, setUser } = useUser();

  useEffect(() => {
    const findUser = async () => {
      const [error, latesUser] = await userById(user.id);

      if (error === null) {
        storageSave(STORAGE_KEY_USER, latesUser);

        setUser(latesUser);
      }
    };

    findUser();
  }, [setUser, user.id]);

  return (
    <>
      <ProfileHeader username={user.username}></ProfileHeader>
      <ProfileTranslationHistory
        translations={user.translations}
      ></ProfileTranslationHistory>
      <ProfileActions></ProfileActions>
    </>
  );
};
export default withAuth(Profile);
