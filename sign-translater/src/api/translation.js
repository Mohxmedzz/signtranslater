import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL_TRANSLATION

export const translationAdd = async (user, translation) => {
try {
    /*Fetching to the url but specifying which id */
    const response = await fetch(`${apiUrl}/${user.id}`, {
        /*We want it to be patch bcz we are updating an existing record not adding or deleting */
        method: 'PATCH',
        headers: createHeaders(),
        body: JSON.stringify({
            translations: [...user.translations, translation]
        })
    })
    if (!response.ok) {
        throw new Error('Could not update the translation')
    }
    const result = await response.json()
    return [null, result]
} catch (error) {
    return [ error.message, null]
}
}
export const translationClearHistory = async (userId) =>{
    try {
        const response = await fetch(`${apiUrl}/${userId}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })    
        if (!response.ok) {
            throw new Error('Could not update translations')
        }
        const result = await response.json()
        return [ null, result ]    
    } catch (error) {
        return [ error.message, null ]
    }
}