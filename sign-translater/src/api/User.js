import { createHeaders } from './index';

const apiUrl = process.env.REACT_APP_API_URL_TRANSLATION

/*
Not adding anything to the database, only reading from the database/api
Sort of private doesn't need to export 
*/
const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if(!response.ok){
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        return [ null, data ]
    } catch (error) {
        return [ error.message, [] ]
    }
}
/*
Needs authentication before creating resources
Sort of private functions doesn't need to export
*/
const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST', /*Create a resource*/
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })

        })
        if(!response.ok){
            throw new Error('Could not create user with username ' + username)
        }
        const data = await response.json()
        return [ null, data ]
    } catch (error) {
        return [ error.message, [] ]
    }
}
/*
Will be used by the component so has to export 
*/
export const loginUser = async (username) => {
    const [ checkError, user ] = await checkForUser(username)
    /*
    if something goes wrong we return this error
    */
    if (checkError !== null) {
        return [ checkError, null ]
    }
    /*
    if nothing goes wrong we return the user we have already
    pop helps to return the last item in the array
    */
    if (user.length > 0) {
        return [ null, user.pop() ]
    }
    /*
    if you can't find the user we have to create a new user 
    */
    return await createUser(username)

}

export const userById = async (userId) => {
    try {
        const response = await fetch (`${apiUrl}/${userId}`)
        if(!response.ok){
            throw new Error('Could not fetch user')
        }
        const user = await response.json()
        return [null, user]
    }
    catch(error){
        return [error.message, null ]

    }
}

export const translationAdd = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation]
            })
        })
        if(!response.ok){
            throw new Error('Ajja bajja, no translation')
        }
        const result = await response.json()
        return [null, result ]
    }
    catch(error){
        return  [error.message, null]

    }

}