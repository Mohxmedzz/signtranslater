const apiKey = process.env.REACT_APP_API_KEY

export const createHeaders = () => {
    return {
        'Content-Type': 'application/json', /*JSON data being sent to my api */
        'x-api-key': apiKey /*Key we specified in .env  */
    }
}