import { NavLink } from "react-router-dom";
import { useUser } from "../../store/UserContext";
import "./Comp.css";
import pageIcon from "../../images/icon.png";
import signOut from "../../images/signOut.png";
import signedIn from "../../images/signedIn.png";
import { storageDelete } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
const Navbar = ({ username }) => {
  const { user, setUser } = useUser();
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure?")) {
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
    }
  };
  return (
    <nav className="navbarCss config myNav">
      <div>
        <img className="iconCss" src={pageIcon} alt="logo"></img>
        <NavLink to="/translate">Lost In Translation</NavLink>
      </div>
      {user !== null && (
        <ul>
          <label className="singedIn">
            <NavLink to="/profile" className="signedIn">
              {user.username}
              <img className="iconCss" src={signedIn} alt="signedIn"></img>
            </NavLink>
          </label>
          <button className="signOut" onClick={handleLogoutClick}>
            <img className="iconCss" src={signOut} alt="signOut"></img>
          </button>
        </ul>
      )}
    </nav>
  );
};
export default Navbar;
