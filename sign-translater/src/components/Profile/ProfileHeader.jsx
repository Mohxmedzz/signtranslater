import React from "react";
import "./Profile.css";
const ProfileHeader = ({ username }) => {
  return (
    <header>
      <div className="profileHeader">
        <h1>Profile {username}</h1>
      </div>
    </header>
  );
};

export default ProfileHeader;
