import { translationClearHistory } from "../../api/translation";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../store/UserContext";
import { storageSave } from "../../utils/storage";
import "./Profile.css";

const ProfileActions = () => {
  const { user, setUser } = useUser();

  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?\nThis can not be undone!")) {
      return;
    }
    const [clearError] = await translationClearHistory(user.id);

    if (clearError !== null) {
      return;
    }
    /*clear translations and set the states */
    const updateUser = {
      ...user,
      translations: [],
    };
    storageSave(STORAGE_KEY_USER, updateUser);
    setUser(updateUser);
  };

  return (
    <ul className="historyBtn">
      <li>
        <li>
          <button className="btn" onClick={handleClearHistoryClick}>
            Clear history
          </button>
        </li>
      </li>
    </ul>
  );
};
export default ProfileActions;
