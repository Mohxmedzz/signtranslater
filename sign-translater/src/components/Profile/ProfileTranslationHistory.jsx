import ProfileTranslationsHistoryItem from "./ProfileTranslationHistoryItem";
import "./Profile.css";

const ProfileTranslationHistory = ({ translations }) => {
  const translationList = translations.map((translation, index) => (
    <ProfileTranslationsHistoryItem
      key={index + "-" + translation}
      translation={translation}
    />
  ));
  console.log(translationList.slice(-10));

  return (
    <section>
      <div className="historyTitle">
        <h4>Your translation history</h4>
      </div>
      <div className="historySubtitle">
        {translationList.length === 0 && <p>You have no translation yet.</p>}

        <ul>{translationList.slice(-10)}</ul>
      </div>
    </section>
  );
};
export default ProfileTranslationHistory;
