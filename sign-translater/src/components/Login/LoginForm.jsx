import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../store/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { storageSave } from "../../utils/storage";
import "./loginForm.css";
import arrow from "../../images/arrow.png";
import logo from "../../images/icon.png";

const usernameConfig = {
  required: true,
  minLength: 3,
};

const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  /*local state */
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  /*side effects */
  /*empty dependencies - only run once*/
  useEffect(() => {
    if (user !== null) {
      navigate("translate");
    }
  }, [user, navigate]);

  /*event handlers */

  /*
async await makes us not need to use fetch .then (await does it in the background)
if there is no error, error will be null otherwise error will be displayed
if there is no user, user will be null otherwise user will be displayed
*/
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };
  /*Render functions*/
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username === "required") {
      return <span>Username is required</span>;
    }
    if (errors.username === "minLength") {
      return <span>Username is too short (min. 3)</span>;
    }
  })();

  return (
    <>
      <div className="loginText">
        <div className="divImg">
          <img src={logo} alt="logo" className="logo" />
        </div>
        <div className="divTitle">
          <h2 className="removeMargin">Lost in Translation</h2>
          <p>Get started</p>
        </div>
      </div>
      <form className="formLog" onSubmit={handleSubmit(onSubmit)}>
        <div className="loginForm">
          <input
            className="loginInput"
            type="text"
            placeholder="What's your name?"
            {...register("username", usernameConfig)}
          />
          {errorMessage}
          <button className="loginButton" type="submit" disabled={loading}>
            <img src={arrow} className="arrow" alt="arrow" />
          </button>
        </div>
        {loading && <p>Logging in...</p>}
        {apiError && <p> {apiError} </p>}
      </form>
    </>
  );
};
export default LoginForm;
