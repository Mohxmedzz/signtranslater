import React, { useState } from "react";
import { useForm } from "react-hook-form";
import SignForm from "./SignForm";
import "./Translation.css";
import arrow from "../../images/arrow.png";

const TranslationForm = ({ onTranslate }) => {
  const { register, handleSubmit } = useForm();

  const [translation, setTranslation] = useState(null);

  const onSubmit = ({ translateWord }) => {
    //only letters and space
    let regEx = /^[a-z][a-z\s]*$/;
    if (translateWord.match(regEx)) {
      onTranslate(translateWord);
      setTranslation(translateWord);
    } else {
      alert("Only letters and space");
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="translationDiv">
        <input
          className="TranslateInput"
          type="text"
          placeholder="What do you want to translate?"
          maxlength="40"
          {...register("translateWord")}
        />
        <button className="translateButton" typ="submit">
          <img src={arrow} className="arrow" alt="arrow" />
        </button>
      </div>
      <div className="signLanguage">
        {translation ? <SignForm translateWord={translation} /> : ""}
      </div>
    </form>
  );
};

export default TranslationForm;
