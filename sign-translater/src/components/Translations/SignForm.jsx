import React from "react";

function SignForm({ translateWord }) {
  translateWord = translateWord.toLowerCase();
  let translateArray = translateWord.split("");

  //set value to empty array for the space img
  translateArray.forEach(function (item, i) {
    if (item === " ") translateArray[i] = "blanc-";
  });

  //merge single array value with the remaining link to img import
  translateArray = translateArray.map((x) => "img/" + x + ".png");
  return (
    <>
      <section>
        <fieldset>
          {translateArray.map((item) => {
            return <img className="signWords" src={item} alt="   "></img>;
          })}
        </fieldset>
      </section>
    </>
  );
}

export default SignForm;
