# Sign language translater

This is a simple sign language translator-app using React Framework.
It will store a new user when logged in.

Once logged in you will be navigated to the translation page, where you can type in english words and small sentences(max 40 char) and receive the translation in american sign language.

All translations will be added to the API and the latest 10 translations will be shown on the profile page where you have the option to clear the history(once you have cleared the history you won't be able to undone it).  

You can navgigate between the pages and sign out through the navbar.

## Planning 

In the folder "FimgaImg" in our project you will find our project planning and structure over our component tree and design from figma.

## Install

```
git clone https://gitlab.com/Mohxmedzz/signtranslater.git
cd sign-translater
npm install
```

## Connect with heroku api

Create new file named `.env` in the public file and type in (replace the `#` with your key, and the url location):  
REACT_APP_API_KEY=`###`
REACT_APP_API_URL_TRANSLATION=`###`

## Usage

```
npm start
```

Run npm start in the terminal, it will start the application and open the browser at `localhost:3000`.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Contributors 

[Mohamed Abdel Monem (@Mohxmedzz)](@Mohxmedzz)
[Victor Wiksell (@VictorWiksell)](@VictorWiksell)

## Contributing

No contributions allowed. 

## Licence 

© 2022, Victor & Mohamed
